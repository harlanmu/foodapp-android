package com.harlanmukdis.foodapp.views.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.harlanmukdis.foodapp.R
import com.harlanmukdis.foodapp.databinding.ItemListFoodBinding
import com.harlanmukdis.foodapp.model.FoodModel

class FoodAdapter(private var context: Context,
                  private var list: List<FoodModel>):
    RecyclerView.Adapter<FoodAdapter.ViewHolder>() {

    private var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemListFoodBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val food = list[position]
        holder.bind(food)
    }

    override fun getItemCount(): Int = list.size

    fun setOnItemClick(listener: OnItemClickListener) {
        this.onItemClickListener = listener
    }

    inner class ViewHolder(private val binding: ItemListFoodBinding):
        RecyclerView.ViewHolder(binding.root) {

            fun bind(food: FoodModel) {
                with(binding) {
                    source = food

                    Glide.with(itemView.context)
                        .load(food.image)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.bg_grey)
                        .into(binding.ivImage)

                    layoutItem.setOnClickListener {
                        onItemClickListener?.onItemClick(food)
                    }
                }
            }
    }

    interface OnItemClickListener {
        fun onItemClick(food: FoodModel)
    }
}