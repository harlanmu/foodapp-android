package com.harlanmukdis.foodapp.views

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.Resources
import android.graphics.Rect
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.harlanmukdis.foodapp.R
import com.harlanmukdis.foodapp.data.DataManager
import com.harlanmukdis.foodapp.databinding.ActivityFoodListBinding
import com.harlanmukdis.foodapp.model.FoodModel
import com.harlanmukdis.foodapp.utils.RetrofitErrorAdapter
import com.harlanmukdis.foodapp.views.adapter.FoodAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlin.math.roundToInt


class FoodListActivity: AppCompatActivity() {
    private lateinit var binding: ActivityFoodListBinding
    private var compositeDisposable: CompositeDisposable? = null
    private var foodAdapter: FoodAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_food_list)

        getFoods()

        binding.swFood.setOnRefreshListener {
            getFoods()
        }
    }

    private fun initList(foods: List<FoodModel>) {
        val manager = GridLayoutManager(this,  2)
        binding.rvFood.layoutManager = manager
        if (binding.rvFood.itemDecorationCount == 0)
            binding.rvFood.addItemDecoration(GridSpacingItemDecoration(2, dpToPx(10), true))
        foodAdapter = FoodAdapter(this, foods)
        binding.rvFood.adapter = foodAdapter
        foodAdapter?.setOnItemClick(object : FoodAdapter.OnItemClickListener {
            override fun onItemClick(food: FoodModel) {
                val intent = Intent(this@FoodListActivity, FoodDetailActivity::class.java)
                intent.putExtra("data", food)
                startActivity(intent)
            }
        })
    }

    @SuppressLint("CheckResult")
    private fun getFoods() {
        binding.swFood.isRefreshing = true
        val disposable = DataManager().can().getFoods()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                binding.swFood.isRefreshing = false
                if (it.isSuccessful) {
                    val type = object : TypeToken<List<FoodModel>>() {}.type
                    val res = Gson().fromJson<List<FoodModel>>(it.body(), type)
                    val foods = arrayListOf<FoodModel>()
                    foods.addAll(res)
                    initList(foods)
                } else {

                }
            }, {
                binding.swFood.isRefreshing = false
                val error = RetrofitErrorAdapter(it)
                Snackbar.make(binding.root, error.message.toString(), Snackbar.LENGTH_LONG).show()
            })
        compositeDisposable?.add(disposable)
    }

    class GridSpacingItemDecoration(
        private val spanCount: Int,
        private val spacing: Int,
        private val includeEdge: Boolean
    ) :
        ItemDecoration() {
        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column
            if (includeEdge) {
                outRect.left =
                    spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right =
                    (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)
                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right =
                    spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }
    }

    private fun dpToPx(dp: Int): Int {
        val r: Resources = resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp.toFloat(),
            r.displayMetrics
        ).roundToInt()
    }
}