package com.harlanmukdis.foodapp.data.remote.retrofit

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.reactivex.Maybe
import retrofit2.Response
import retrofit2.http.GET

interface APIService {
    @GET("foods")
    fun getFoods(): Maybe<Response<JsonArray>>
}