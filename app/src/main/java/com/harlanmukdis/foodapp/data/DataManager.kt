package com.harlanmukdis.foodapp.data

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.harlanmukdis.foodapp.data.remote.FoodAPI
import io.reactivex.Maybe
import retrofit2.Response

class DataManager: DataManagerType {
    private var dm: DataManager? = null

    fun can(): DataManager {
        if (dm == null) {
            dm = DataManager()
        }
        return dm as DataManager
    }

    private val sFoodsAPI: FoodAPI = FoodAPI()

    override fun getFoods(): Maybe<Response<JsonArray>> =
        sFoodsAPI.getFoods()
}