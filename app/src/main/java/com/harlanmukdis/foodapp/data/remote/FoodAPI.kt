package com.harlanmukdis.foodapp.data.remote

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.harlanmukdis.foodapp.FoodApplication
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class FoodAPI : BaseAPI() {
    fun getFoods(): Maybe<Response<JsonArray>> =
        FoodApplication.apiService.getFoods()
            .subscribeOn(Schedulers.io())
}