package com.harlanmukdis.foodapp.data.remote.retrofit

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import com.harlanmukdis.foodapp.BuildConfig

class RetrofitServiceFactory {
    private val BASE_URL: String = BuildConfig.BASE_URL
    private val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
    private val gson = GsonBuilder()
        .registerTypeAdapterFactory(DataTypeAdapterFactory())
        .setLenient()
        .create()
    private val sBuilder: Retrofit.Builder = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
    var sRetrofit: Retrofit? = null

    fun <S> createService(serviceClass: Class<S>): S {
        httpClient.addInterceptor { ResponseInterceptor().intercept(it) }
        val lClient: OkHttpClient = httpClient.build()
        httpClient.connectTimeout(150, TimeUnit.SECONDS)
        httpClient.readTimeout(150, TimeUnit.SECONDS)
        httpClient.writeTimeout(150, TimeUnit.SECONDS)
        sRetrofit = sBuilder.client(lClient).build()
        return sRetrofit!!.create(serviceClass)
    }
}