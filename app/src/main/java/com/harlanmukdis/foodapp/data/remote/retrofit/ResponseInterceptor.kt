package com.harlanmukdis.foodapp.data.remote.retrofit

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ResponseInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val lResponse: Response = chain.proceed(chain.request())
        when (lResponse.code) {
            401 -> {
                // unauthorized
                Log.e("interceptor", "401")
            }
            403 -> {
                // forbidden
                Log.e("interceptor", "403")
            }
            404 -> {
                // endpoint not found
                Log.e("interceptor", "404")
            }
            500 -> {
                // internal server error
                Log.e("interceptor", "500")
            }
            502 -> {
                // bad gateway
                Log.d("interceptor", "502")
            }
            503 -> {
                // service unavailable
                Log.e("interceptor", "503")
            }
            504 -> {
                // gateway timeout
                Log.e("interceptor", "504")
            }
        }
        return lResponse
    }
}