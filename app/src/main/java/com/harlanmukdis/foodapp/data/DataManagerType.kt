package com.harlanmukdis.foodapp.data

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.reactivex.Maybe
import retrofit2.Response

interface DataManagerType {
    fun getFoods(): Maybe<Response<JsonArray>>
}