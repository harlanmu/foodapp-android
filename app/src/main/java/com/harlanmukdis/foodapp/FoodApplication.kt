package com.harlanmukdis.foodapp

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.harlanmukdis.foodapp.data.remote.retrofit.APIService
import com.harlanmukdis.foodapp.data.remote.retrofit.RetrofitServiceFactory

class FoodApplication: Application() {
    companion object {
        lateinit var app: FoodApplication
        lateinit var apiService: APIService
    }

    private fun getInstance(): FoodApplication? {
        return app
    }

    fun getContext(): Context? {
        return app
    }

    override fun onCreate() {
        super.onCreate()
        app = this

        apiService = RetrofitServiceFactory().createService(APIService::class.java)
    }

    fun isNetworkAvailable(context: Context? = null): Boolean {
        val lConnectivityManager =
            context?.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val lNetworkInfo = lConnectivityManager.activeNetworkInfo
        return lNetworkInfo != null && lNetworkInfo.isConnected
    }
}