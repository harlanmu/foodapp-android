package com.harlanmukdis.foodapp.model

import java.io.Serializable

data class FoodModel(
    val name: String,
    val image: String,
    val desc: String
): Serializable
