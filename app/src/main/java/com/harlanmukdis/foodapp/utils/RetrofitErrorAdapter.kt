package com.harlanmukdis.foodapp.utils

import android.util.Log
import com.google.gson.JsonObject
import com.harlanmukdis.foodapp.data.remote.retrofit.RetrofitServiceFactory
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.HttpException
import java.io.IOException

class RetrofitErrorAdapter(private val error: Throwable) {
    var errorResponse: JsonObject? = null
        private set
    var message: String? = null
        private set

    // constant variable. feel free to move these to S.java
    private val messageKey = "message"
    private val validationKey = "description"
    private val ioErrorMessage = "Input Output Error. Please Try Again"
    private val unknownErrorMessage = "Unknown Error. Please Try Again"
    private fun convert() {
        when (error) {
            is HttpException -> {
                val lResponse = error.response()
                val lConverter: Converter<ResponseBody?, JsonObject> = RetrofitServiceFactory().sRetrofit!!
                    .responseBodyConverter(JsonObject::class.java, arrayOfNulls<Annotation>(0))
                try {
                    when {
                        lResponse!!.code() == 400 -> {
                            message = parseMessage(validationKey)
                        }
                        lResponse.code() == 401 -> {
                            // unauthorized
                            Log.e(RetrofitErrorAdapter::class.java.simpleName, "interceptor : 401")
                            message = parseMessage(validationKey)
                        }
                        lResponse.code() == 403 -> {
                            // forbidden
                            Log.e(RetrofitErrorAdapter::class.java.simpleName, "forbidden : 403")
                            message = parseMessage(validationKey)
                        }
                        lResponse.code() == 404 -> {
                            // endpoint not found
                            Log.e(RetrofitErrorAdapter::class.java.simpleName, "endpoint not found : 404")
                            message = "endpoint " + lResponse.message()
                        }
                        lResponse.code() == 500 -> {
                            // internal server error
                            Log.e(
                                RetrofitErrorAdapter::class.java.simpleName,
                                " internal server error : 500"
                            )
                            message = lResponse.message()
                        }
                        lResponse.code() == 502 -> {
                            // bad gateway
                            Log.d(RetrofitErrorAdapter::class.java.simpleName, "bad gateway : 502")
                            message = lResponse.message()
                        }
                        lResponse.code() == 503 -> {
                            // service unavailable
                            Log.e(RetrofitErrorAdapter::class.java.simpleName, "service unavailable : 503")
                            message = lResponse.message()
                        }
                        lResponse.code() == 504 -> {
                            // gateway timeout
                            Log.e(RetrofitErrorAdapter::class.java.simpleName, "gateway timeout : 504")
                            message = lResponse.message()
                        }
                        lResponse.code() == 508 -> {
                            // socket timeout
                            Log.e(RetrofitErrorAdapter::class.java.simpleName, "socket timeout : 504")
                            message = lResponse.message()
                        }
                        else -> {
                            errorResponse = lConverter.convert(lResponse.errorBody())
                            message = parseMessage(messageKey)
                        }
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            is IOException -> {
                message = ioErrorMessage
            }
            else -> {
                message = unknownErrorMessage
            }
        }
    }

    private fun parseMessage(key: String?): String {
        return if (errorResponse!![key] == null) unknownErrorMessage else errorResponse!![key].toString()
    }

    init {
        convert()
    }
}